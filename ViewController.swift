//
//  ViewController.swift
//  SwiftScrollView
//
//  Created by Jason Vantrease on 1/19/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //let screenSize = CGRect(x: 0, y: 0, width: 1500, height: 1500)
        
        let frameWidth = self.view.frame.size.width
        let frameHeight = self.view.frame.size.height
        let scrollView = UIScrollView(frame:self.view.bounds)
        scrollView.isPagingEnabled = true
        self.view.addSubview(scrollView)
        
        scrollView.contentSize = CGSize(width: self.view.frame.size.width * 3, height: self.view.frame.size.height * 3)
        
        //0,0
        let greenView = UIView(frame: CGRect(x: 0, y: 0, width: frameWidth, height: frameHeight))
        greenView.backgroundColor = UIColor.green
        scrollView.addSubview(greenView)
        //0,1
        let blueView = UIView(frame: CGRect(x: 0, y: frameHeight, width: frameWidth, height: frameHeight))
        blueView.backgroundColor = UIColor.blue
        scrollView.addSubview(blueView)
        //0,2
        let redView = UIView(frame: CGRect(x: 0, y: frameHeight*2, width: frameWidth, height: frameHeight))
        redView.backgroundColor = UIColor.red
        scrollView.addSubview(redView)
        // 1,0
        let blackView = UIView(frame: CGRect(x: frameWidth, y: 0, width: frameWidth, height: frameHeight))
        blackView.backgroundColor = UIColor.black
        scrollView.addSubview(blackView)
        // 1,1
        let grayView = UIView(frame: CGRect(x: frameWidth, y: frameHeight, width: frameWidth, height: frameHeight))
        grayView.backgroundColor = UIColor.gray
        scrollView.addSubview(grayView)
        // 1,2
        let orangeView = UIView(frame: CGRect(x: frameWidth, y: frameHeight*2, width: frameWidth, height: frameHeight))
        orangeView.backgroundColor = UIColor.orange
        scrollView.addSubview(orangeView)
        // 2,0
        let brownView = UIView(frame: CGRect(x: frameWidth*2, y: 0, width: frameWidth, height: frameHeight))
        brownView.backgroundColor = UIColor.brown
        scrollView.addSubview(brownView)
        // 2,1
        let cyanView = UIView(frame: CGRect(x: frameWidth*2, y: frameHeight, width: frameWidth, height: frameHeight))
        cyanView.backgroundColor = UIColor.cyan
        scrollView.addSubview(cyanView)
        // 2,2
        let yellowView = UIView(frame: CGRect(x: frameWidth*2, y: frameHeight*2, width: frameWidth, height: frameHeight))
        yellowView.backgroundColor = UIColor.yellow
        scrollView.addSubview(yellowView)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

